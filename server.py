import logging
import os

from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS

from src.utils.logger import setup_logging_json

LOGGER = logging.getLogger(__name__)


def init_app():
    load_dotenv()
    application = Flask(__name__)
    # TODO: need to be check enable CORS
    CORS(application, resources={r"/*": {"origins": "*", "send_wildcard": "False"}})
    from src.routers.routes import bp, add_routes
    add_routes()
    application.register_blueprint(bp)
    return application


if __name__ == "__main__":
    if os.environ.get('env') == 'prd':
        setup_logging_json()
    app = init_app()
    # TODO improve handler exception here
    # exception_handler.handler_exception(api)
    host = os.environ.get("HOST", "0.0.0.0")
    port = os.environ.get("PORT", 3000)
    debug = os.environ.get("DEBUG", False)
    app.run(host=host, port=port, debug=debug)
