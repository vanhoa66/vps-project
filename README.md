# truyen-tranh

## [Tiến độ](./Tasks.md)

## DB design
![Beautiful design](./docs/images/truyen-tranh.png)

## Getting start
- To Do: 
    - .flake8
    - pre-commit-config.yaml
    - gitlab-ci.yml
    - Dockerfile
    - pytest.ini
    - Considering about create `venv` in every project rather than use mode global


- Before all tạo database `truyen-tranh` trước.
- Tạo 3 table: trong file [schema.sql](./src/db/migrations/schema.sql) 

- Create docker volume first: `docker volume create pddata`.
- Create tables in `./db/migrations/schema.sql`.
- `docker-compose up`.