# syntax=docker/dockerfile:1
FROM python:3.8-alpine
ENV POSTGRES_DNS=postgresql://postgres:password@db:5432/truyen-tranh
WORKDIR /code
RUN apk add --no-cache gcc musl-dev linux-headers postgresql-dev
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
EXPOSE 3000
COPY . .
CMD ["python3", "server.py"]