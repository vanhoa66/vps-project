import os
import json
import logging
from pathlib import Path

from logging.config import dictConfig


def setup_logging_json(
        default_path="logging.json",
        default_level=logging.WARNING,
        env_key="LOG_CFG"
):
    Path("logs").mkdir(parents=True, exist_ok=True)
    path = os.environ.get(env_key, default_path)
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
            dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
