from flask import Blueprint
from flask_restful import Api

from src.http.handlers.categories.create import CreateCategoryHandler
from src.http.handlers.categories.update import UpdateCategoryHandler
from src.http.handlers.categories.delete import DeleteCategoryHandler
from src.http.handlers.categories.read_all import GetAllCategoriesHandler

from src.http.handlers.stories.create import CreateStoryHandler
from src.http.handlers.stories.update import UpdateStoryHandler
from src.http.handlers.stories.delete import DeleteStoryHandler
from src.http.handlers.stories.read_all import GetAllStoryHandler

from src.http.handlers.chapters.create import CreateChapterHandler
from src.http.handlers.chapters.update import UpdateChapterHandler
from src.http.handlers.chapters.read_all import GetAllChaptersHandler
from src.http.handlers.chapters.delete import DeleteChapterHandler

bp = Blueprint("api", __name__)
api = Api(bp)


def add_routes():
    api.add_resource(CreateCategoryHandler, '/categories')
    api.add_resource(UpdateCategoryHandler, '/categories/<category_id>')
    api.add_resource(GetAllCategoriesHandler, '/categories/list')
    api.add_resource(DeleteCategoryHandler, '/categories/<category_id>')

    api.add_resource(CreateStoryHandler, '/stories')
    api.add_resource(UpdateStoryHandler, '/stories/<story_id>')
    api.add_resource(GetAllStoryHandler, '/stories/list')
    api.add_resource(DeleteStoryHandler, '/stories/<story_id>')

    api.add_resource(CreateChapterHandler, '/chapters')
    api.add_resource(UpdateChapterHandler, '/chapters/<chapter_id>')
    api.add_resource(GetAllChaptersHandler, '/chapters/list')
    api.add_resource(DeleteChapterHandler, '/chapters/<chapter_id>')
