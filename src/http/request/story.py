from cerberus import Validator


class ModelStoryValidator:
    def __init__(self, payload):
        self.payload = payload

    @staticmethod
    def __get_rules():
        return {
            "name": {"type": "string", "required": True, "minlength": 1},
            "categories": {"type": "list", "required": False, "minlength": 1},
            "slug": {"type": "string", "required": True, "minlength": 1},
            "description": {"type": "string", "required": False, "minlength": 1},
            "title_seo": {"type": "string", "required": True, "minlength": 1},
            "desc_seo": {"type": "string", "required": True, "minlength": 1},
            "image": {"type": "string", "required": True, "minlength": 1},
            "author": {"type": "string", "required": True, "minlength": 1},
            "genres": {"type": "string", "required": False, "minlength": 1},
            "visible": {"type": "boolean", "required": False},
            "popular": {"type": "boolean", "required": False},
            "completed": {"type": "boolean", "required": False}
        }

    @staticmethod
    def __get_rules_update():
        return {
            "name": {"type": "string", "required": False, "minlength": 1},
            "categories": {"type": "list", "required": False, "minlength": 1},
            "slug": {"type": "string", "required": False, "minlength": 1},
            "description": {"type": "string", "required": False, "minlength": 1},
            "title_seo": {"type": "string", "required": False, "minlength": 1},
            "desc_seo": {"type": "string", "required": False, "minlength": 1},
            "image": {"type": "string", "required": False, "minlength": 1},
            "author": {"type": "string", "required": False, "minlength": 1},
            "genres": {"type": "string", "required": False, "minlength": 1},
            "visible": {"type": "boolean", "required": False},
            "popular": {"type": "boolean", "required": False},
            "completed": {"type": "boolean", "required": False}
        }

    def validate_create(self):
        validator = Validator()
        rules = self.__get_rules()
        valid = validator.validate(document=self.payload, schema=rules)
        errors = validator.errors
        return valid, self.payload, errors

    def validate_update(self):
        validator = Validator()
        rules = self.__get_rules_update()
        valid = validator.validate(document=self.payload, schema=rules)
        errors = validator.errors
        return valid, self.payload, errors
