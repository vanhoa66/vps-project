from cerberus import Validator


class ModelChapValidator:
    def __init__(self, payload):
        self.payload = payload

    @staticmethod
    def __get_rules_create():
        return {
            "name": {"type": "string", "required": True, "minlength": 1},
            "stories": {"type": "list", "required": True, "minlength": 1},
            "slug": {"type": "string", "required": True, "minlength": 1},
            "content": {"type": "string", "required": True, "minlength": 1},
            "visible": {"type": "boolean", "required": False},
        }

    @staticmethod
    def __get_rules_update():
        return {
            "name": {"type": "string", "required": False, "minlength": 1},
            "stories": {"type": "list", "required": False, "minlength": 1},
            "slug": {"type": "string", "required": False, "minlength": 1},
            "content": {"type": "string", "required": False, "minlength": 1},
            "visible": {"type": "boolean", "required": False}
        }

    def validate_create(self):
        validator = Validator()
        rules = self.__get_rules_create()
        valid = validator.validate(document=self.payload, schema=rules)
        errors = validator.errors
        return valid, self.payload, errors

    def validate_update(self):
        validator = Validator()
        rules = self.__get_rules_update()
        valid = validator.validate(document=self.payload, schema=rules)
        errors = validator.errors
        return valid, self.payload, errors
