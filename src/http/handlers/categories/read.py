# from flask_restful import Resource
#
# from src.services.categories import CategoryService
# from src.repositories.category import CategoryRepository
#
# category_repo = CategoryRepository()
# category_service = CategoryService(category_repo)
#
#
# class GetAllCategoriesHandler(Resource):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.category_service = category_service
#
#     def get(self):
#         try:
#             categories = self.category_service.get()
#             result = [dict(ct) for ct in categories]
#             return {"status": 200, "message": result}, 200
#         except Exception as error:
#             print("GET ALL categories error")
#             print(error)
#             return {"status": 500, "message": "WHOOP something wrong!"}, 500
#
