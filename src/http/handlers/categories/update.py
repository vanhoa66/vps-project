import json

from flask import request
from flask_restful import Resource

from src.http.request.category import ModelCategoryValidator
from src.repositories.category import CategoryRepository
from src.services.categories import CategoryService

category_repo = CategoryRepository()
category_service = CategoryService(category_repo)


class UpdateCategoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.category_service = category_service

    def put(self, category_id):
        status = 400
        request_body = json.loads(request.get_data() or "{}")
        category_validator = ModelCategoryValidator(request_body)
        valid, payload, errors = category_validator.validate_update()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"
            }
        success, message = self.category_service.update(category_id, payload)
        status = 500
        if success:
            status = 200
        return {"status": status, "message": message}, status
