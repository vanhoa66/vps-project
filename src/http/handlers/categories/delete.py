from flask_restful import Resource

from src.repositories.category import CategoryRepository
from src.services.categories import CategoryService

category_repo = CategoryRepository()
category_service = CategoryService(category_repo)


class DeleteCategoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.category_service = category_service

    def delete(self, story_id):
        success, message = self.chapter_service.delete(story_id)
        status = 400
        if success:
            status = 200
        return {"status": status, "message": message}, status
