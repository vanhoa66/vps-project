
from flask_restful import Resource

from src.repositories.category import CategoryRepository
from src.services.categories import CategoryService

category_repo = CategoryRepository()
category_service = CategoryService(category_repo)


class GetAllCategoriesHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.category_service = category_service

    def get(self):
        try:
            categories = self.category_service.get_all()
            if categories:
                categories = [dict(ct) for ct in categories]
            return {"status": 200, "message": categories}, 200
        except Exception as error:
            print("GET ALL categories error")
            print(error)
            return {"status": 500, "message": "WHOOP something wrong!"}, 500
