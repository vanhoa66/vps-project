import json
from flask import request
from flask_restful import Resource

from src.entities.categories import Category
from src.services.categories import CategoryService
from src.repositories.category import CategoryRepository
from src.http.request.category import ModelCategoryValidator


category_repo = CategoryRepository()
category_service = CategoryService(category_repo)


class CreateCategoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.category_service = category_service

    def post(self):
        status = 400
        request_body = json.loads(request.get_data() or "{}")
        category_validator = ModelCategoryValidator(request_body)
        valid, payload, errors = category_validator.validate_create()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"
            }
        category = Category(**payload)
        success, message = self.category_service.create(category)
        status = 500
        if success:
            status = 201
        return {"status": status, "message": message}, status
