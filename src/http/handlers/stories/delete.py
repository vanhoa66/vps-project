from flask_restful import Resource

from src.repositories.story import StoryRepository
from src.services.stories import StoryService

story_repo = StoryRepository()
story_service = StoryService(story_repo)


class DeleteStoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.story_service = story_service

    def delete(self, story_id):
        success, message = self.story_service.delete(story_id)
        status = 400
        if success:
            status = 200
        return {"status": status, "message": message}, status
