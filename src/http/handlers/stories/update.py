import json

from flask import request
from flask_restful import Resource

from src.http.request.story import ModelStoryValidator
from src.repositories.story import StoryRepository
from src.services.stories import StoryService

story_repo = StoryRepository()
story_service = StoryService(story_repo)


class UpdateStoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.story_service = story_service

    def put(self, story_id):
        status = 400
        request_body = json.loads(request.get_data() or "{}")
        story_validator = ModelStoryValidator(request_body)
        valid, payload, errors = story_validator.validate_update()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"
            }
        success, message = self.story_service.update(story_id, payload)
        status = 500
        if success:
            status = 200
        return {"status": status, "message": message}, status
