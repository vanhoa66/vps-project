from flask_restful import Resource


from src.repositories.story import StoryRepository
from src.services.stories import StoryService

story_repo = StoryRepository()
story_service = StoryService(story_repo)


class GetAllStoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.story_service = story_service

    def get(self):
        status = 200
        try:
            stories = self.story_service.get_all()
            if stories:
                stories = [dict(st) for st in stories]
            return {"status": status, "message": stories}, status
        except Exception as error:
            status = 500
            print("GET ALL stories error")
            print(error)
            return {"status": status, "messages": "Whoop! something wrong!"}, status
