import json

from flask import request
from flask_restful import Resource

from src.entities.stories import Story
from src.http.request.story import ModelStoryValidator
from src.repositories.story import StoryRepository
from src.services.stories import StoryService

story_repo = StoryRepository()
story_service = StoryService(story_repo)


class CreateStoryHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.story_service = story_service

    def post(self):
        status = 400
        request_body = json.loads(request.get_data() or "{}")
        story_validator = ModelStoryValidator(request_body)
        valid, payload, errors = story_validator.validate_create()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"
            }
        story = Story(**payload)
        success, message = self.story_service.create(story)
        status = 500
        if success:
            status = 201
        return {"status": status, "message": message}, status
