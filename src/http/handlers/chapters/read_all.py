from flask_restful import Resource


from src.repositories.chapter import ChapterRepository
from src.services.chapters import ChaptersService

chapter_repo = ChapterRepository()
chapter_service = ChaptersService(chapter_repo)


class GetAllChaptersHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chapter_service = chapter_service

    def get(self):
        status = 200
        try:
            chapters = self.chapter_service.get_all()
            if chapters:
                chapters = [dict(ct) for ct in chapters]
            return {"status": status, "message": chapters}, status
        except Exception as error:
            status = 500
            print("GET ALL stories error")
            print(error)
            return {"status": status, "messages": "Whoop! something wrong!"}, status
