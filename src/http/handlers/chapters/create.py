import json

from flask import request
from flask_restful import Resource

from src.entities.chapters import Chapter
from src.http.request.chapter import ModelChapValidator
from src.repositories.chapter import ChapterRepository
from src.services.chapters import ChaptersService

chapter_repo = ChapterRepository()
chapter_service = ChaptersService(chapter_repo)


class CreateChapterHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chapter_service = chapter_service

    def post(self):
        status = 400
        test = request.get_data()
        request_body = json.loads(request.get_data() or "{}")
        chapter_validator = ModelChapValidator(request_body)
        valid, payload, errors = chapter_validator.validate_create()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"  # TODO move this to const
            }
        chapter = Chapter(**payload)
        success, message = self.chapter_service.create(chapter)
        status = 500
        if success:
            status = 201
        return {"status": status, "message": message}, status
