from flask_restful import Resource

from src.repositories.chapter import ChapterRepository
from src.services.chapters import ChaptersService

chapter_repo = ChapterRepository()
chapter_service = ChaptersService(chapter_repo)


class DeleteChapterHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chapter_service = chapter_service

    def delete(self, chapter_id):
        success, message = self.chapter_service.delete(chapter_id)
        status = 400
        if success:
            status = 200
        return {"status": status, "message": message}, status
