import json

from flask import request
from flask_restful import Resource

from src.http.request.chapter import ModelChapValidator
from src.repositories.chapter import ChapterRepository
from src.services.chapters import ChaptersService

chapter_repo = ChapterRepository()
chapter_service = ChaptersService(chapter_repo)


class UpdateChapterHandler(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.chapter_service = chapter_service

    def put(self, chapter_id):
        status = 400
        request_body = json.loads(request.get_data() or "{}")
        chapter_validator = ModelChapValidator(request_body)
        valid, payload, errors = chapter_validator.validate_update()
        if not valid:
            return {
                "status": status,  # TODO change to const something like: status_HTTP_400
                "errors": errors,
                "message": "Bad request"
            }
        success, message = self.chapter_service.update(chapter_id, payload)
        status = 500
        if success:
            status = 200
        return {"status": status, "message": message}, status
