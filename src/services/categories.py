import uuid
from datetime import datetime

from src.entities.categories import Category


class CategoryService:
    def __init__(self, category_repo):
        self.category_repo = category_repo

    def create(self, category: Category):
        # TODO check category already exists
        now = datetime.now()
        category.created_at = now
        category.updated_at = now
        # TODO recheck id does not auto generate, gen_random_uuid enabled
        category.id = uuid.uuid4().hex
        category_created = self.category_repo.create(category.__dict__)
        if category_created:
            return True, category_created
        return False, "Whoop! something wrong"

    def get_all(self):
        return self.category_repo.get_all()

    def update(self, id: str, data):
        return self.category_repo.update(id, data), "Updated successfully"

    def delete(self, id: str):
        return self.category_repo.delete(id), "Deleted successfully"
