import uuid
from datetime import datetime

from src.entities.stories import Story


class StoryService:
    def __init__(self, story_repo):
        self.story_repo = story_repo

    def create(self, story: Story):
        now = datetime.now()
        story.created_at = now
        story.updated_at = now

        # TODO recheck id  does not auto generate, gen_random_uuid enabled.
        story.id = uuid.uuid4().hex
        story_created = self.story_repo.create(story.__dict__)
        if story_created:
            return True, story_created
        return False, "Whoop! something wrong"

    def get_all(self):
        return self.story_repo.get_all()

    def update(self, id: str, data):
        return self.story_repo.update(id, data), "Updated successfully"

    def delete(self, id: str):
        return self.story_repo.delete(id), "Deleted successfully"
