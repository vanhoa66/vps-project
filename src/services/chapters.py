import uuid
from datetime import datetime

from src.entities.chapters import Chapter


class ChaptersService:
    def __init__(self, chapter_repo):
        self.chapter_repo = chapter_repo

    def create(self, chapter: Chapter):
        # TODO check chapter already exists first
        now = datetime.now()
        chapter.created_at = now
        chapter.updated_at = now

        # TODO recheck id does not auto generate, gen_random_uuid enabled

        chapter.id = uuid.uuid4().hex
        chapter_created = self.chapter_repo.create(chapter.__dict__)
        if chapter_created:
            return True, chapter_created
        return False, "Whoop! something wrong"

    def get_all(self):
        return self.chapter_repo.get_all()

    def update(self, id: str, data):
        return self.chapter_repo.update(id, data), "Updated successfully"

    def delete(self, id: str):
        return self.chapter_repo.delete(id), "Deleted successfully"
