# load pgcrrypto run gen_random_uuid()
CREATE EXTENSION pgcrypto;

CREATE TABLE IF NOT EXISTS categories (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR,
    slug VARCHAR UNIQUE,
    description VARCHAR,
    title_seo VARCHAR,
    desc_seo VARCHAR,
    image VARCHAR,
    created_at DATE,
    updated_at DATE,
    deleted_at DATE,
    visible BOOLEAN
)


CREATE TABLE IF NOT EXISTS stories (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    categories VARCHAR [],
    name VARCHAR,
    slug VARCHAR UNIQUE,
    description VARCHAR,
    title_seo VARCHAR,
    desc_seo VARCHAR,
    image VARCHAR,
    author VARCHAR,
    genres VARCHAR,
    popular BOOLEAN,
    completed BOOLEAN,
    created_at DATE,
    updated_at DATE,
    deleted_at DATE,
    visible BOOLEAN
)

CREATE TABLE IF NOT EXISTS chapters (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    stories VARCHAR[],
    name VARCHAR,
    slug VARCHAR UNIQUE,
    content text,
    created_at DATE,
    updated_at DATE,
    deleted_at DATE,
    visible BOOLEAN
)

