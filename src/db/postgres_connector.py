import os

import psycopg2


class PostgresConnector:
    __instance = None
    db = None

    def __init__(self):
        self.__connect__()

    def __connect__(self):
        postgres_dns = os.environ.get('POSTGRES_DNS', None)
        if not postgres_dns:
            print('Has no POSTGRES DNS config')
            exit("Missing POSTGRES DNS")
        self.db = psycopg2.connect(
            dsn=postgres_dns,
            keepalives=1,
            keepalives_idle=30,
            keepalives_interval=10,
            keepalives_count=5,
        )

    @staticmethod
    def get_instance():
        if PostgresConnector.__instance is None:
            PostgresConnector.__instance = PostgresConnector()
        return PostgresConnector.__instance
