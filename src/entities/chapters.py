class Chapter(object):
    def __init__(self,
                 id=None,
                 stories=None,
                 name=None,
                 slug=None,
                 content=None,
                 visible=None,
                 created_at=None,
                 updated_at=None,
                 deleted_at=None
                 ):
        self.id = id
        self.stories = stories
        self.name = name
        self.slug = slug
        self.content = content
        self.visible = visible
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at

    def dict_to_json(self):
        return {
            "id": self.id,
            "stories": self.stories,
            "name": self.name,
            "slug": self.slug,
            "content": self.content,
            "visible": self.visible,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "deleted_at": self.deleted_at
        }
