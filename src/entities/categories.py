class Category(object):
    def __init__(self,
                 id=None,
                 name=None,
                 slug=None,
                 description=None,
                 title_seo=None,
                 desc_seo=None,
                 image=None,
                 created_at=None,
                 updated_at=None,
                 deleted_at=None,
                 visible=True
                 ):
        self.id = id
        self.name = name
        self.slug = slug
        self.description = description
        self.title_seo = title_seo
        self.desc_seo = desc_seo
        self.image = image
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
        self.visible = visible

    def dict_to_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "slug": self.slug,
            "desc": self.description,
            "title_seo": self.title_seo,
            "desc_seo": self.desc_seo,
            "image": self.image,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "deleted_at": self.deleted_at,
            "visible": self.visible,
        }
