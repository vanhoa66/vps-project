class Story(object):
    def __init__(self,
                 id=None,
                 categories=None,
                 name=None,
                 slug=None,
                 description=None,
                 title_seo=None,
                 desc_seo=None,
                 image=None,
                 author=None,
                 genres=None,
                 created_at=None,
                 updated_at=None,
                 deleted_at=None,
                 visible=None,
                 popular=None,
                 completed=None,
                 ):
        self.id = id
        self.categories = categories
        self.name = name
        self.slug = slug
        self.description = description
        self.title_seo = title_seo
        self.desc_seo = desc_seo
        self.image = image
        self.author = author
        self.genres = genres
        self.created_at = created_at
        self.updated_at = updated_at
        self.deleted_at = deleted_at
        self.visible = visible
        self.popular = popular
        self.completed = completed

    def dict_to_json(self):
        return {
            "id": self.id,
            "categories": self.categories,
            "name": self.name,
            "slug": self.slug,
            "desc": self.description,
            "title_seo": self.title_seo,
            "desc_seo": self.desc_seo,
            "image": self.image,
            "author": self.author,
            "genres": self.genres,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "deleted_at": self.deleted_at,
            "visible": self.visible,
            "popular": self.popular,
            "completed": self.completed
        }
