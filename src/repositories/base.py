import psycopg2.extras


class BaseRepository:
    def apply(self, sql, returning=False):
        cursor = self.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute(sql)
            self.db.commit()
            if returning:
                result = cursor.fetchone()[0]
                return result
            return True
        except Exception as error:
            print(error)
            self.db.rollback()
            if error.pgcode == '23505':
                return error.pgerror
            raise error
        finally:
            cursor.close()

    @staticmethod
    def build_query(data):
        if not isinstance(data, dict):
            raise ValueError("data not is a dict")
        columns = []
        insert_query = []
        update_query = []

        values = list(data.values())
        keys = list(data.keys())

        for key in keys:
            columns.append("%s" % key)
        for (index, value) in enumerate(values):
            if not value:
                insert_query.append("%s" % "NULL")
                update_query.append("%s = '%s'" % (keys[values.index(value)], value))
            else:
                if type(value) == list:
                    value = "{%s}" % ','.join(value)
                insert_query.append("'%s'" % value)
                update_query.append("%s = '%s'" % (keys[index], value))
        return {
            "columns": ", ".join(columns),
            "insert_query": ", ".join(insert_query),
            "update_query": ", ".join(update_query),
        }

    def create(self, data: dict):
        query_builder = self.build_query(data)
        columns = query_builder.get("columns")
        insert = query_builder.get("insert_query")
        sql = "INSERT INTO %s (%s) VALUES (%s) RETURNING id" % (
            self.__table_name__,
            columns,
            insert,
        )

        return self.apply(sql, returning=True)

    def get_all(self):
        cursor = self.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            # TODO critical RECHECK SQL HERE
            sql = f"""
                SELECT id, name, slug, title_seo, description, desc_seo, image 
                FROM {self.__table_name__} 
                WHERE deleted_at IS NULL"""
            if self.__table_name__ == 'chapters':
                sql = f"""
                        SELECT id, stories, name, slug, content 
                        FROM {self.__table_name__} 
                        WHERE deleted_at IS NULL"""
            cursor.execute(sql)
            result = cursor.fetchall()
            return result
        except Exception as error:
            print("SELECT * FROM  ERROR")
            print(error)
        finally:
            cursor.close()

    def get(self, id: str) -> dict:
        cursor = self.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        try:
            cursor.execute(
                f"""SELECT * FROM {self.__table_name__} WHERE id=%s AND deleted_at IS NULL""", (id,),
            )
            result = cursor.fetchone()
            return result
        except Exception as error:
            print("SELECT * FROM BY ID ERROR")
            print(error)
        finally:
            cursor.close()

    def update(self, id, data: dict):
        query_builder = self.build_query(data)
        update_str = query_builder.get("update_query")
        sql = "UPDATE %s SET %s WHERE id ='%s'" % (
            self.__table_name__,
            update_str,
            id
        )
        return self.apply(sql)

    def delete(self, id: str):
        sql = "DELETE FROM %s WHERE id = '%s' " % (self.__table_name__, id)
        return self.apply(sql)
