from src.db.postgres_connector import PostgresConnector
from src.repositories.base import BaseRepository


class CategoryRepository(BaseRepository):
    __table_name__ = "categories"

    def __init__(self, **kwargs) -> None:
        self.db = PostgresConnector.get_instance().db
        self.__dict__.update(kwargs)
