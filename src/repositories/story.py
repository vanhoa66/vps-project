from src.db.postgres_connector import PostgresConnector
from src.repositories.base import BaseRepository


class StoryRepository(BaseRepository):
    # TODO move table name to const
    __table_name__ = "stories"

    def __init__(self, **kwargs) -> None:
        self.db = PostgresConnector.get_instance().db
        self.__dict__.update(kwargs)
