# What did I do?

## Week 1

### Do:
- Khởi tạo project structure
- Tạo method create `category`
### Plan(những thứ làm tuần 2):
- Tạo method `update`, `delete` cho `category`
- Dockerfile
- Cập nhật ReadMe.md

### Improve:
- Đang dùng postgres thuần tự generate câu lệnh ( nên chuyển qua dùng ORM)

